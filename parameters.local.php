<?php
$params = '
{
    "debug":                false,
    "httpsEnabled":         true,
    "databaseEnabled":      false,
'.
    //path informations
'   "rootURL":              "localhost/souvlaki",
    "rootPath":             "/srv/http/souvlaki",
    "indexModule":          "index",
'.
    //database
'   "databaseHost":         "localhost",
    "databasePort":         3306,
    "databaseName":         "souvlaki",
    "databaseUser":         "root",
    "databasePassword":     "root",
    "databaseTablePrefix":  "souvlaki_",
'.
    //root access : 3jk3V8qf
'   "passwordHash":         "$2y$12$mluhsGSLMQOC55L6djRgI.nNXz66shA0MHGXgGn81wiynv3jyvCDy",
    "passwordHashCost":     12,
'.
    //Misc
'   "salt":                 "",
'.
    //contact
'   "mailKWAM":             "nomail@nomail.com",
    "websiteName":          "Souvlaki framework",
    "mailOwner":            "owner@mail.fr"
}
';
?>

<?php

use Lib\IO;
use Lib\Conf;

try
{
	include_once(Conf::$rootPath . "/modules/core/header/controller.php");
	include_once("view.php");
}
catch (Exception $e)
{
	IO::displayException($e);
}

?>

# Souvlaki php framework #

# Documentation #

## install ##

Edit the following files to fit to your server:
- `.htaccess`
- `parameters.local.php`

In production, you can rename `parameters.local.php` by `parameters.php`

## organization ##

### engine ###
`/libraries`
Diverse abstract classes

`/log`
log repository, in standby

`/modules/core`
Essential core modules such as header, footer, error, etc...
Now there is no Public folder anymore in the core modules, if need, it has to be located in /Public

### editable `/modules` ###

`/Public`
Everything at the root of /Public/ is reachable by direct url. The place to store js, css, images etc...
The base content (favicon.ico, style.css and form.js) is called by /modules/core/header in the first place. Any other regular module can override it

`/modules`
modules whose name match with url provided on the browser (e.g. http(s)://mywebsite.com/mymodule)
If a module has the same name than a core module (`/modules/core`) it will override its execution by your own

By default, souvlaki come with 3 basic modules, like any module, you can add a `model.php` and a `Public` directory if needed:

`/modules/header`
visual header (call the core header)

`/modules/footer`
visual footer (call the core footer)

`/modules/index`
root website content (call header and footer regular modules)

## what is a module ? ##

A module is compound of 1 to 4 files :

- controller.php *entry point*
- view.php *optional (html)*
- model.php *optional (data processing)*
- api.php *optional entry point for POST, GET and PUT requests*

- Public *optional, where to put raw files like js, css, html or whatever*

Public folder access by module : `http(s)://website.com/moduleName?Public=your/path/in/Public/folder/myFile.js`

General Public folder access : `http(s)://website.com/Public/your/path/to/myFile.js`

Traditional MVC (Model View Controller), the controller is always the first called file, then it calls the model where the data are processed and call view.php to prompt the result, most of the time, it gonna wrap it between the core modules header and footer.

## Libraries ##

Libraries are content in the `/libraries` folder with the extension .lib.php , it is always one abstract class per file (per lib) which is possible to call at anytime by any code in the framework

### SQL `/libraries/sql.lib.php` ###

- First of all:

```
use Lib\SQL;

$SQL = new SQL('table-name');
```

- request conditions:
    - `$SQL->join('table-to-join', 'column1', 'column2');`
    - `$SQL->setWhere('index', 'value');`
    - `$SQL->setWhereNot('index', 'value');`
    - `$SQL->orderby('column', 'order');`

- request goal:
    - `$SQL->select();`
    - `$SQL->insert();`
    - `$SQL->update();`

- debug:
    - `$SQL->echo_request(type)` with type = 'insert' or 'update'
    - `$SQL->query('SQL query')` NOT SECURE, usage only for fast debug

# writing conventions #

## General ##

- `Conf::$a-var-from-the-parameters.php` , available at anytime unless you forget to write `use Lib\Conf;` at the beginning of your file ;)

Inside a module :

- controller.php :
    - `$CtoV['module-name']` is the array which pass the data from the Controler to the View

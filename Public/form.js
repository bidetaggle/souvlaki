window.onload = function(){
	var parameters = {
		success: "#success",
		error: "#error",
		errorClass: "wrong"	//class html de mise en surbriance du champs où il y a l'erreur
	};

	if(document.getElementsByTagName("form").length != 0){ //to avoid console warning in case of there is no form on the page
		$("form.ajax").submit(function(e){
			e.preventDefault();

			var formData = new FormData($(this)[0]);
			let form = $(this);
			let deleteAction = form.attr('action').match(/[:a-z\/]+\.php\?delete/);

			if(deleteAction)
				if(!confirm('Do you really want to remove this forever ? へ[ •́ ‸ •̀ ]ʋ'))
					return;

			function readData(response){
				var champs = {};
				var success = true;
				console.log(response);

				if(deleteAction && response == "true"){
					form.fadeOut();
				}
				else{
					let Response = $.parseJSON(response);
					//liste de tous les champs (input/textarea) du formulaire et mise à jour des erreurs
					$.each(Response, function(tagName, value){
						if(tagName != "Location"){
							//affiche ou non les erreurs en fonction de leur nom
							var errTag = $(parameters.error + " ." + tagName);
							//selectionne l'input (avec le name ou la class) concerné pour ajouter le style si l'erreur existe
							var input = $("[name='"+tagName+"'], ."+tagName+":not("+parameters.error+" ."+tagName+")")
							if(!value){
								success = false;
								errTag.fadeIn();
								input.addClass(parameters.errorClass);
							}
							else{
								errTag.fadeOut();
								input.removeClass(parameters.errorClass);
							}
						}
					});

					if(success){
						if(Response.Location)
							document.location.href = Response.Location;
						else
							document.location.href = "";
					}
				}
			}

			$.ajax({
			    url: this.action,
			    type: 'POST',
			    data: formData,
			    async: false,
			    cache: false,
			    contentType: false,
			    processData: false,
			    success: function (response) {
			    	readData(response);
			    }
			});

			/* old version */
			
			// $.post(this.action, $(this).serialize())
			// .done(function(response){
			// 	var champs = {};
			// 	var success = true;
			// 	console.log(response);
			//
			// 	if(deleteAction && response == "true"){
			// 		form.fadeOut();
			// 	}
			// 	else{
			// 		let Response = $.parseJSON(response);
			// 		//liste de tous les champs (input/textarea) du formulaire et mise à jour des erreurs
			// 		$.each(Response, function(tagName, value){
			// 			if(tagName != "Location"){
			// 				//affiche ou non les erreurs en fonction de leur nom
			// 				var errTag = $(parameters.error + " ." + tagName);
			// 				//selectionne l'input (avec le name ou la class) concerné pour ajouter le style si l'erreur existe
			// 				var input = $("[name='"+tagName+"'], ."+tagName+":not("+parameters.error+" ."+tagName+")")
			// 				if(!value){
			// 					success = false;
			// 					errTag.fadeIn();
			// 					input.addClass(parameters.errorClass);
			// 				}
			// 				else{
			// 					errTag.fadeOut();
			// 					input.removeClass(parameters.errorClass);
			// 				}
			// 			}
			// 		});
			//
			// 		if(success){
			// 			if(Response.Location)
			// 				document.location.href = Response.Location;
			// 			else
			// 				document.location.href = "";
			// 		}
			// 	}
			// });
		});
	}
};
